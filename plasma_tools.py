#!/usr/bin/env python
# coding: utf-8

seed_val = 7
import os
os.environ['PYTHONHASHSEED'] = str(seed_val)
import random
random.seed(seed_val)
import numpy as np
np.random.seed(seed_val)
from scipy.optimize import fsolve as fsolver
from matplotlib import pyplot as plt
import matplotlib.colors as mcolors
import astropy.constants as aconst
import pfsspy
import pfsspy.tracing as tracing








def nearest(items, pivot):
    ''' 
    This function returns the object in 'items' that is the closest to the object 'pivot'.
    '''
    found = min(items, key=lambda x: abs(x - pivot))
    return found



def cartesian_to_spherical(x, y, z):
    ''' 
    Converts cartesian to spherical coords .. physics convention.
    Suitable for the MAS MHD Model.
    
    phi         ==> azimuth angle (lon): east-west position, [-pi, pi] rad --> [-180, 180] deg .. [0, 360] deg in MAS.
    theta       ==> polar (elevation) angle (lat): north-south position, [0, pi] rad --> [0, 180] deg .. [-90, 90] deg in MAS.
    r           ==> radial distance[0, inf] --> [1, 30] Rs in MAS.
    
    Convert angles from radians to degrees
    theta_degrees = np.degrees(theta)
    phi_degrees = np.degrees(phi)
    '''
    # Calculate the radial distance r
    r = np.sqrt(x**2 + y**2 + z**2)
    # Calculate the polar angle theta
    theta = np.arccos(z/r)
    # Calculate the azimuthal angle phi
    phi = np.arctan2(y, x)
    
    return phi, theta, r



def newkirk_f(r):
    ''' 
    Newkirk electron-density model; fundamental emission.
    e.g. `fold` is a multiplicative factor to change the density scaling.
    '''
    fold = 1
    return fold*4.2e4*10.**(4.32/r)



def newkirk_h(r):
    ''' 
    Newkirk electron-density model; harmonic emission.
    e.g. `fold` is a multiplicative factor to change the density scaling.
    '''
    fold = 2
    return fold*4.2e4*10.**(4.32/r)



def omega_pe_r(ne_r, r):
    ''' 
    Plasma frequency density relationship. 
    Works only for the fundamental emission. 
    ''' 
    return 8.93e3*(ne_r(r))**(0.5)*2*np.pi



def freq_to_R(f_pe, ne_r=newkirk_h):
    """
    Starting height for a wave frequency 
    """
    func = lambda R: f_pe - (omega_pe_r(ne_r, R))/2/np.pi
    R_solution = fsolver(func, 1.5) # solve the R
    return R_solution # [R_s]



def distance_3d(x, y, z, x0, y0, z0):
    """
    3d distance from a point and a line 
    Source: https://stackoverflow.com/questions/67571967/python-find-closest-point-to-3d-point-on-3d-spline 
    """
    dx = x - x0
    dy = y - y0
    dz = z - z0
    d = np.sqrt(dx**2 + dy**2 + dz**2)
    return d



def min_distance(x, y, z, P, precision=5): 
    """
    Compute minimum/a distance/s between
    a point P[x0,y0,z0] and a curve (x,y,z)
    rounded at `precision`.
    
    ARGS:
        x, y, z   (array)
        P         (3dtuple)
        precision (integer)
        
    Returns min indexes and distances array. 
    Source: https://stackoverflow.com/questions/67571967/python-find-closest-point-to-3d-point-on-3d-spline 
    """
    # compute distance 
    d = distance_3d(x, y, z, P[0], P[1], P[2])
    d = np.round(d, precision)
    # find the minima 
    glob_min_idxs = np.argwhere(d==np.min(d)).ravel()
    return glob_min_idxs, d



def get_colors(n):
    '''
    Generate a list of (n) hex colors.
    '''
    cmap = plt.get_cmap('rainbow')
    colors = [cmap(i) for i in np.linspace(0, 1, n)]
    hex_colors = [mcolors.to_hex(c) for c in colors]
    hex_colors[0] = '#0000ff' # set first color to blue
    hex_colors[-1] = '#ff0000' # set last color to red
    return hex_colors




def plot_pfss_3d(pfss=False, rotate_view=None, save=False):
    '''
    This function does the following:
    - Plot the Sun in 3D;
    - Plot the FULL PFSS magnetic field lines to the radio sources;
    - Plot the Sun-Earth LOS.
    
    Inputs:
        - Must be already defined in the workspace.
    
    Output:
        - 3D figure includes all the above.
    '''

    fig = plt.figure(figsize=[8,8])
    ax = fig.add_subplot(111, projection='3d', computed_zorder=False)

    # draw the Sun
    theta, phi = np.mgrid[0:np.pi:100j, 0:2*np.pi:100j]
    x = np.sin(theta)*np.cos(phi) 
    y = np.sin(theta)*np.sin(phi) 
    z = np.cos(theta)
    ax.plot_surface(x, y, z, color='orange', zorder=0)
    
    if pfss:
        # draw the PFSS field lines
        nrho = 100
        rss = 4 
        num_footpoints_lat = 40
        num_footpoints_lon = 60
        r = 1.2*const.radius

        pfss_in = pfsspy.Input(gong_map, nrho, rss)
        pfss_out = pfsspy.pfss(pfss_in)
        lat = np.linspace(np.radians(-89), np.radians(89), num_footpoints_lat, endpoint=False)
        lon = np.linspace(np.radians(0), np.radians(359), num_footpoints_lon, endpoint=False)
        lat, lon = np.meshgrid(lat, lon, indexing='ij')
        lat, lon = lat.ravel()*u.rad, lon.ravel()*u.rad
        seeds = SkyCoord(lon, lat, r, frame=pfss_out.coordinate_frame)
        tracer = tracing.FortranTracer(max_steps=20000, step_size=1)
        field_lines = tracer.trace(seeds, pfss_out)

        with tqdm(total=len(seeds), desc='Plotting the field lines') as pbar:
            for n, field_line in enumerate(field_lines):
                color = {0:'black', -1:'tab:blue', 1:'tab:red'}.get(field_line.polarity)
                coords = field_line.coords
                coords.representation_type = 'cartesian'
                x_line = coords.x/const.radius
                y_line = coords.y/const.radius
                z_line = coords.z/const.radius

                if field_line.polarity==1 or field_line.polarity==-1:
                    opacity = 0.4
                else:
                    opacity = 0.1

                ax.plot3D(x_line, y_line, z_line, alpha=0.3, zorder=1, color=color, linewidth=1)
                pbar.update(1)
    
    # draw Sun-Earth LOS direction
    x_pos = 2
    y_pos = 0
    z_pos = 0
    x_direct = 1
    y_direct = 0
    z_direct = 0
    ax.quiver(x_pos, y_pos, z_pos, x_direct, y_direct, z_direct, length=2, arrow_length_ratio=0.3, color='black')
    ax.text(x_pos, y_pos, z_pos, 'Earth LOS', size=10, zorder=2, color='black')

    # plot settings
    ax.grid(False)
    ax.set_xlim(-3, 3)
    ax.set_ylim(-3, 3)
    ax.set_zlim(-3, 3)
    ax.set_box_aspect([3, 3, 3])
    for axis in 'xyz':
        getattr(ax, 'set_{}label'.format(axis))(axis+' [R$_\odot]$')
    if rotate_view == 'front':
        ax.view_init(elev=0, azim=0)
    elif rotate_view == 'right':
        ax.view_init(elev=0, azim=90)
    elif rotate_view == 'left':
        ax.view_init(elev=0, azim=-90)
    elif rotate_view == 'top':
        ax.view_init(elev=90, azim=0)
    elif rotate_view == 'angle':
        ax.view_init(elev=0, azim=-35)
    else:
        pass
    if save:
        plt.savefig(f'{basedir_lof}/plots/pfss/pfss3d_rotated_{rotate_view}.png', dpi=300, format='png', bbox_inches='tight')
        plt.savefig(f'{basedir_lof}/plots/pfss/pfss3d_rotated_{rotate_view}.pdf', format='pdf', bbox_inches='tight')
    plt.tight_layout()
    plt.show()





def plot_full_fieldlines_3d(rotate_view=None, fit=False, save=False):
    '''
    This function does the following:
    - Plot the Sun in 3D;
    - Plot the FULL PFSS magnetic field lines;
    - Plot the contours of radio emissions in 3D;
    - Plot the Sun-Earth LOS.
    
    Inputs:
        - Must be already defined in the workspace.
    
    Output:
        - 3D figure includes all the above.
    '''

    fig = plt.figure(figsize=[10,10])
    ax = fig.add_subplot(111, projection='3d', computed_zorder=False)
    
    # draw half of the Sun
    theta, phi = np.mgrid[0:np.pi:100j, 0:np.pi:100j]
    x = np.sin(theta)*np.cos(phi) 
    y = np.sin(theta)*np.sin(phi) 
    z = np.cos(theta)
    with tqdm(total=len(z), desc='Plotting the Sun') as pbar:
        # applying rotation matrix to rotate the half of the Sun 
        Rx = np.array([[1, 0, 0],
                       [0, np.cos(np.deg2rad(0)), -np.sin(np.deg2rad(0))],
                       [0, np.sin(np.deg2rad(0)), np.cos(np.deg2rad(0))]])
        x, y, z = np.einsum('ij,jkl->ikl', Rx, np.array([x, y, z]))
        Rz = np.array([[np.cos(np.deg2rad(-90)), -np.sin(np.deg2rad(-90)), 0],
                       [np.sin(np.deg2rad(-90)), np.cos(np.deg2rad(-90)), 0],
                       [0, 0, 1]])
        x, y, z = np.einsum('ij,jkl->ikl', Rz, np.array([x, y, z]))
        ax.plot_surface(x, y, z, color='orange', zorder=0)
        pbar.update(1)
    
    # draw the PFSS field lines
    nrho = 50
    rss = 4
    num_footpoints = 40
    r = 1.2*aconst.R_sun
    
    pfss_in = pfsspy.Input(gong_map, nrho, rss)
    pfss_out = pfsspy.pfss(pfss_in)
    lat = np.linspace(np.radians(-89), np.radians(89), num_footpoints, endpoint=False)
    lon = np.linspace(np.radians(0), np.radians(359), num_footpoints*2, endpoint=False)
    lat, lon = np.meshgrid(lat, lon, indexing='ij')
    lat, lon = lat.ravel()*u.rad, lon.ravel()*u.rad
    seeds = SkyCoord(lon, lat, r, frame=pfss_out.coordinate_frame)
    tracer = tracing.FortranTracer(max_steps=20000, step_size=1)
    field_lines = tracer.trace(seeds, pfss_out)
    
    with tqdm(total=len(field_lines), desc='Plotting field lines') as pbar:
        for n, field_line in enumerate(field_lines):
            color = {0: 'black', -1: 'tab:blue', 1: 'tab:red'}.get(field_line.polarity)
            coords = field_line.coords
            coords.representation_type = 'cartesian'
            x_line = coords.x/aconst.R_sun
            y_line = coords.y/aconst.R_sun
            z_line = coords.z/aconst.R_sun
            ax.plot3D(x_line, y_line, z_line, alpha=0.1, color=color, linewidth=1)
            pbar.update(1)

    # draw the radio sources
    # to store the coords of the spheres of the radio sources
    x_radiosource, y_radiosource, z_radiosource = [], [], []
    cpsX, cpsY, cpsZ = [], [], []
    
    with tqdm(total=len(centroids), desc='Plotting radio sources') as pbar:
        for i in range(len(centroids)):
            xshift = xshift_arcsec.value[i]/Rsun_arcsec
            yshift = yshift_arcsec.value[i]/Rsun_arcsec
            # calc the height from Newkirk model, in Rs 
            rmodel = freq_to_R(f_pe=freqs[i]*1e6, ne_r=newkirk)[0]
            # calc the height on the plane of sky (POS), in Rs 
            rpos = np.sqrt(xshift**2 + yshift**2)
            # calc the distance from the POS, in Rs
            zshift = np.sqrt(rmodel**2 - rpos**2) # --> same form as in the Badman's paper

            # make the radio spheres using the beam size
            lofar_cleaned = IM.IMdata()
            lofar_cleaned.load_fits(filtered_lof_imgs_sort[i])
            beam = lofar_cleaned.get_beam()
            b_major = beam[0]
            b_minor = beam[1]
            radius = np.sqrt(b_major**2 + b_minor**2)

            theta_src, phi_src = np.mgrid[0:np.pi:20j, 0:2*np.pi:20j]
            x_src = radius*np.sin(theta_src)*np.cos(phi_src) + zshift
            y_src = radius*np.sin(theta_src)*np.sin(phi_src) + xshift
            z_src = radius*np.cos(theta_src) + yshift
            surf = ax.plot_surface(x_src, y_src, z_src, color=gradiant_colors[i], alpha=0.3, zorder=20, 
                                   label=f'{freqs[i]} MHz, {obs_time[i]} UT')
            surf._facecolors2d = surf._facecolor3d
            surf._edgecolors2d = surf._edgecolor3d

            x_radiosource.append(x_src)
            y_radiosource.append(y_src)
            z_radiosource.append(z_src)

            # draw centers of radio sources (cp: center point) 
            cp1 = np.median(x_src)
            cp2 = np.median(y_src)
            cp3 = np.median(z_src)
            ax.plot3D(cp1, cp2, cp3, marker='o', markersize=3, color=gradiant_colors[i])
            cpsX.append(cp1)
            cpsY.append(cp2)
            cpsZ.append(cp3)
            pbar.update(1)

    ## gather the points to be fitted 
    #cpsX.insert(0, x_point) # list of known x coordinates 
    #cpsY.insert(0, y_point) # list of known y coordinates 
    #cpsZ.insert(0, z_point) # list of known z coordinates 

    ## drop the coords of the AR
    #cpsX = cpsX[1:]
    #cpsY = cpsY[1:]
    #cpsZ = cpsZ[1:]
    
    if fit:
        # Generate function out of provided points
        tck, u_param = splprep([cpsX, cpsY, cpsZ], k=3, s=20)
        # Creating spline points 
        newPoints = splev(u_param, tck)
        # draw the spline fit curve, represents the trajectory
        ax.plot3D(newPoints[:][0], newPoints[:][1], newPoints[:][2], 'r--')

    # draw Sun-Earth LOS direction
    x_pos = 2
    y_pos = 0
    z_pos = 0
    x_direct = 1
    y_direct = 0
    z_direct = 0
    ax.quiver(x_pos, y_pos, z_pos, x_direct, y_direct, z_direct, length=2, arrow_length_ratio=0.3, color='black')
    ax.text(x_pos, y_pos, z_pos, 'Earth LOS', size=10, zorder=1, color='black')

    # plot settings
    ax.legend(title='Centroids', loc='upper left', prop={'size':8}, bbox_to_anchor=(-0.01, 1))
    ax.grid(False)
    ax.set_xlim(0, 4)
    ax.set_ylim(-3, 3)
    ax.set_zlim(-3, 3)
    ax.set_box_aspect([2, 3, 3])
    for axis in 'xyz':
        getattr(ax, 'set_{}label'.format(axis))(axis+' [R$_\odot]$')
    if rotate_view == 'front':
        ax.view_init(elev=0, azim=0)
    elif rotate_view == 'right':
        ax.view_init(elev=0, azim=90)
    elif rotate_view == 'left':
        ax.view_init(elev=0, azim=-90)
    elif rotate_view == 'top':
        ax.view_init(elev=90, azim=0)
    else:
        pass
    if save:
        plt.savefig(f'{basedir_lof}/plots/pfss/pfss3d_contours_{burst_num.split("_")[0]}_rotated_{rotate_view}.png', dpi=300, format='png', bbox_inches='tight')
        plt.savefig(f'{basedir_lof}/plots/pfss/pfss3d_contours_{burst_num.split("_")[0]}_rotated_{rotate_view}.pdf', format='pdf', bbox_inches='tight')
    plt.show()



def plot_pfss_3d(rotate_view=None, save=False):
    '''
    This function does the following:
    - Plot the Sun in 3D;
    - Plot the FULL PFSS magnetic field lines to the radio sources;
    - Plot the Sun-Earth LOS.
    
    Inputs:
        - Must be already defined in the workspace.
    
    Output:
        - 3D figure includes all the above.
    '''

    fig = plt.figure(figsize=[8,8])
    ax = fig.add_subplot(111, projection='3d', computed_zorder=False)

    # draw the Sun
    theta, phi = np.mgrid[0:np.pi:100j, 0:2*np.pi:100j]
    x = np.sin(theta)*np.cos(phi) 
    y = np.sin(theta)*np.sin(phi) 
    z = np.cos(theta)
    ax.plot_surface(x, y, z, color='orange', zorder=0)
    
    # draw the PFSS field lines
    nrho = 100
    rss = 4 
    num_footpoints_lat = 40
    num_footpoints_lon = 60
    r = 1.2*const.radius
    
    pfss_in = pfsspy.Input(gong_map, nrho, rss)
    pfss_out = pfsspy.pfss(pfss_in)
    lat = np.linspace(np.radians(-89), np.radians(89), num_footpoints_lat, endpoint=False)
    lon = np.linspace(np.radians(0), np.radians(359), num_footpoints_lon, endpoint=False)
    lat, lon = np.meshgrid(lat, lon, indexing='ij')
    lat, lon = lat.ravel()*u.rad, lon.ravel()*u.rad
    seeds = SkyCoord(lon, lat, r, frame=pfss_out.coordinate_frame)
    with tqdm(total=len(seeds), desc='Tracing the field lines') as pbar:
        tracer = tracing.FortranTracer(max_steps=20000, step_size=1)
        field_lines = tracer.trace(seeds, pfss_out)
        pbar.update(1)
    
    with tqdm(total=len(seeds), desc='Plotting the field lines') as pbar:
        for n, field_line in enumerate(field_lines):
            color = {0: 'black', -1: 'tab:blue', 1: 'tab:red'}.get(field_line.polarity)
            coords = field_line.coords
            coords.representation_type = 'cartesian'
            x_line = coords.x/const.radius
            y_line = coords.y/const.radius
            z_line = coords.z/const.radius
            ax.plot3D(x_line, y_line, z_line, alpha=0.3, zorder=1, color=color, linewidth=1)
            pbar.update(1)
    
    # draw Sun-Earth LOS direction
    x_pos = 2
    y_pos = 0
    z_pos = 0
    x_direct = 1
    y_direct = 0
    z_direct = 0
    ax.quiver(x_pos, y_pos, z_pos, x_direct, y_direct, z_direct, length=2, arrow_length_ratio=0.3, color='black')
    ax.text(x_pos, y_pos, z_pos, 'Earth LOS', size=10, zorder=2, color='black')

    # plot settings
    ax.grid(False)
    ax.set_xlim(-3, 3)
    ax.set_ylim(-3, 3)
    ax.set_zlim(-3, 3)
    ax.set_box_aspect([3, 3, 3])
    for axis in 'xyz':
        getattr(ax, 'set_{}label'.format(axis))(axis+' [R$_\odot]$')
    if rotate_view == 'front':
        ax.view_init(elev=0, azim=0)
    elif rotate_view == 'right':
        ax.view_init(elev=0, azim=90)
    elif rotate_view == 'left':
        ax.view_init(elev=0, azim=-90)
    elif rotate_view == 'top':
        ax.view_init(elev=90, azim=0)
    elif rotate_view == 'angle':
        ax.view_init(elev=0, azim=-35)
    else:
        pass
    if save:
        plt.savefig(f'{basedir_lof}/plots/pfss/pfss3d_rotated_{rotate_view}.png', dpi=300, format='png', bbox_inches='tight')
        plt.savefig(f'{basedir_lof}/plots/pfss/pfss3d_rotated_{rotate_view}.pdf', format='pdf', bbox_inches='tight')
    plt.tight_layout()
    plt.show()




def plot_pfss_halfsun_3d(rotate_view=None, save=False):
    '''
    This function does the following:
    - Plot the Sun in 3D;
    - Plot the FULL PFSS magnetic field lines to the radio sources;
    - Plot the Sun-Earth LOS.
    
    Inputs:
        - Must be already defined in the workspace.
    
    Output:
        - 3D figure includes all the above.
    '''

    fig = plt.figure(figsize=(8,8))
    ax = fig.add_subplot(111, projection='3d', computed_zorder=False)

    # draw half of the Sun
    theta, phi = np.mgrid[0:np.pi:100j, 0:np.pi:100j]
    x = np.sin(theta)*np.cos(phi) 
    y = np.sin(theta)*np.sin(phi) 
    z = np.cos(theta)

    # applying rotation matrix to rotate the half of the Sun
    Rx = np.array([[1, 0, 0],
                   [0, np.cos(np.deg2rad(0)), -np.sin(np.deg2rad(0))],
                   [0, np.sin(np.deg2rad(0)), np.cos(np.deg2rad(0))]])
    x, y, z = np.einsum("ij,jkl->ikl", Rx, np.array([x, y, z]))
    Rz = np.array([[np.cos(np.deg2rad(-90)), -np.sin(np.deg2rad(-90)), 0],
                   [np.sin(np.deg2rad(-90)), np.cos(np.deg2rad(-90)), 0],
                   [0, 0, 1]])
    x, y, z = np.einsum("ij,jkl->ikl", Rz, np.array([x, y, z]))
    ax.plot_surface(x, y, z, color='orange', zorder=0)
    
    # draw the PFSS field lines
    nrho = 40
    rss = 4 
    num_footpoints_lat = 20
    num_footpoints_lon = 30
    r = 1.2*const.radius
    
    pfss_in = pfsspy.Input(gong_map, nrho, rss)
    pfss_out = pfsspy.pfss(pfss_in)
    lat = np.linspace(np.radians(-89), np.radians(89), num_footpoints_lat, endpoint=False)
    lon = np.linspace(np.radians(0), np.radians(359), num_footpoints_lon, endpoint=False)
    lat, lon = np.meshgrid(lat, lon, indexing='ij')
    lat, lon = lat.ravel()*u.rad, lon.ravel()*u.rad
    seeds = SkyCoord(lon, lat, r, frame=pfss_out.coordinate_frame)
    with tqdm(total=len(seeds), desc='Tracing the field lines') as pbar:
        tracer = tracing.FortranTracer(max_steps=20000, step_size=1)
        field_lines = tracer.trace(seeds, pfss_out)
    pbar.update(1)
    
    with tqdm(total=len(seeds), desc='Plotting the field lines') as pbar:
        for n, field_line in enumerate(field_lines):
            color = {0: 'black', -1: 'tab:blue', 1: 'tab:red'}.get(field_line.polarity)
            coords = field_line.coords
            coords.representation_type = 'cartesian'
            x_line = coords.x/const.radius
            y_line = coords.y/const.radius
            z_line = coords.z/const.radius
            ax.plot3D(x_line, y_line, z_line, alpha=0.3, zorder=1, color=color, linewidth=1)
        pbar.update(1)
    
    # draw Sun-Earth LOS direction
    x_pos = 2
    y_pos = 0
    z_pos = 0
    x_direct = 1
    y_direct = 0
    z_direct = 0
    ax.quiver(x_pos, y_pos, z_pos, x_direct, y_direct, z_direct, length=2, arrow_length_ratio=0.3, color='black')
    ax.text(x_pos, y_pos, z_pos, 'Earth LOS', size=10, zorder=2, color='black')

    # plot settings
    ax.grid(False)
    ax.set_xlim(-3, 3)
    ax.set_ylim(-3, 3)
    ax.set_zlim(-3, 3)
    ax.set_box_aspect([3, 3, 3])
    for axis in 'xyz':
        getattr(ax, 'set_{}label'.format(axis))(axis+' [R$_\odot]$')
    if rotate_view == 'front':
        ax.view_init(elev=0, azim=0)
    elif rotate_view == 'right':
        ax.view_init(elev=0, azim=90)
    elif rotate_view == 'left':
        ax.view_init(elev=0, azim=-90)
    elif rotate_view == 'top':
        ax.view_init(elev=90, azim=0)
    elif rotate_view == 'angle':
        ax.view_init(elev=0, azim=-35)
    else:
        pass
    if save:
        plt.savefig(f'{basedir_lof}/plots/pfss/pfss3d_rotated_{rotate_view}.png', dpi=300, format='png', bbox_inches='tight')
        plt.savefig(f'{basedir_lof}/plots/pfss/pfss3d_rotated_{rotate_view}.pdf', format='pdf', bbox_inches='tight')
    plt.tight_layout()
    plt.show()






def plot_nearest_fieldlines_3d(n_nearest_flines=30, pfss=True, fit=False, save=False, 
                              # rotate_view=None,
                              elev=None, azim=None
                              ):
    '''
    This function does the following:
    - Plot the Sun in 3D;
    - Plot the contours of radio emissions in 3D;
    - Plot the NEAREST PFSS magnetic field lines to the radio sources;
    - Plot the Sun-Earth LOS.
    
    Inputs:
        - Must be already defined in the workspace.
    
    Output:
        - 3D figure includes all the above.
    '''

    fig = plt.figure(figsize=(8,8))
    ax = fig.add_subplot(111, projection='3d', computed_zorder=False)

    # draw half of the Sun
    theta, phi = np.mgrid[0:np.pi:100j, 0:np.pi:100j]
    x = np.sin(theta)*np.cos(phi) 
    y = np.sin(theta)*np.sin(phi) 
    z = np.cos(theta)

    # applying rotation matrix to rotate the half of the Sun
    Rx = np.array([[1, 0, 0],
                   [0, np.cos(np.deg2rad(0)), -np.sin(np.deg2rad(0))],
                   [0, np.sin(np.deg2rad(0)), np.cos(np.deg2rad(0))]])
    x, y, z = np.einsum("ij,jkl->ikl", Rx, np.array([x, y, z]))
    Rz = np.array([[np.cos(np.deg2rad(-90)), -np.sin(np.deg2rad(-90)), 0],
                   [np.sin(np.deg2rad(-90)), np.cos(np.deg2rad(-90)), 0],
                   [0, 0, 1]])
    x, y, z = np.einsum("ij,jkl->ikl", Rz, np.array([x, y, z]))
    ax.plot_surface(x, y, z, color='orange', zorder=0)
    
    if pfss:
        # draw the PFSS field lines
        nrho = 50 #90
        rss = 6 #4
        num_footpoints = 60
        r = 1.2*aconst.R_sun

        pfss_in = pfsspy.Input(gong_map, nrho, rss)
        pfss_out = pfsspy.pfss(pfss_in)
        lat = np.linspace(np.radians(-89), np.radians(89), num_footpoints+20, endpoint=False) # num=40
        lon = np.linspace(np.radians(0), np.radians(359), num_footpoints+30, endpoint=False) # num=80
        lat, lon = np.meshgrid(lat, lon, indexing='ij')
        lat, lon = lat.ravel()*u.rad, lon.ravel()*u.rad
        seeds = SkyCoord(lon, lat, r, frame=pfss_out.coordinate_frame)
        with tqdm(total=len(seeds), desc='Tracing the field lines') as pbar:
            tracer = tracing.FortranTracer(max_steps=20000, step_size=1)
            field_lines = tracer.trace(seeds, pfss_out)
            pbar.update(1)
    
    # draw the radio sources
    # to store the coords of the spheres of the radio sources
    x_radiosource, y_radiosource, z_radiosource = [], [], []
    # to store the plotted field lines
    x_lines, y_lines, z_lines = [], [], []
    cpsX, cpsY, cpsZ = [], [], []
    
    with tqdm(total=len(centroids), desc='Plotting the radio sources and the field lines') as pbar:
        for i in range(len(centroids)):
            xshift = xshift_arcsec.value[i]/Rsun_arcsec
            yshift = yshift_arcsec.value[i]/Rsun_arcsec
            # calc the height from Newkirk model, in Rs 
            rmodel = freq_to_R(f_pe=(freqs[i]/2)*1e6, ne_r=newkirk_h)[0]
            # calc the height on the plane of sky (POS), in Rs
            rpos = np.sqrt(xshift**2 + yshift**2)
            # calc the distance from the POS, in Rs
            zshift = np.sqrt(abs(rmodel**2 - rpos**2))

            # make the radio spheres using the beam size
            lofar_cleaned = IM.IMdata()
            lofar_cleaned.load_fits(filtered_lof_imgs_sort[i])
            beam = lofar_cleaned.get_beam()
            b_major = beam[0]
            b_minor = beam[1]
            radius = np.sqrt(b_major**2 + b_minor**2)

            theta_src, phi_src = np.mgrid[0:np.pi:20j, 0:2*np.pi:20j]
            x_src = radius*np.sin(theta_src)*np.cos(phi_src) + zshift
            y_src = radius*np.sin(theta_src)*np.sin(phi_src) + xshift
            z_src = radius*np.cos(theta_src) + yshift
            surf = ax.plot_surface(x_src, y_src, z_src, color=gradiant_colors[i], alpha=0.3, zorder=20, 
                                   label=f'{freqs[i]} MHz, {obs_time[i]} UT')
            surf._facecolors2d = surf._facecolor3d
            surf._edgecolors2d = surf._edgecolor3d

            x_radiosource.append(x_src)
            y_radiosource.append(y_src)
            z_radiosource.append(z_src)

            # draw centers of radio sources (cp: center point)
            cp1 = np.median(x_src)
            cp2 = np.median(y_src)
            cp3 = np.median(z_src)
            ax.plot(cp1, cp2, cp3, marker='o', markersize=3, color=gradiant_colors[i])
            cpsX.append(cp1)
            cpsY.append(cp2)
            cpsZ.append(cp3)

            if pfss:
                # calc the min dist for each field line with every centroid
                info = {'num_fieldline': [], 'min_idx': [], 'min_dist': []}
                for n, field_line in enumerate(field_lines):
                    color = {0:'black', -1:'tab:blue', 1:'tab:red'}.get(field_line.polarity)
                    coords = field_line.coords
                    coords.representation_type = 'cartesian'
                    x_line = coords.x/const.radius
                    y_line = coords.y/const.radius
                    z_line = coords.z/const.radius
                    x = x_line.value
                    y = y_line.value
                    z = z_line.value
                    P = (np.median(x_radiosource[i]), np.median(y_radiosource[i]), np.median(z_radiosource[i]))

                    # store the min dist and the index of that point, along with the num of the field line
                    min_idx, d = min_distance(x, y, z, P)
                    info['num_fieldline'].append(n)
                    info['min_idx'].append(min_idx[0])
                    info['min_dist'].append(d[min_idx][0])

                df = pd.DataFrame(info)

                # show the closest field lines to that radio source
                closest_fieldlines = df.sort_values(by='min_dist', ascending=True).head(n_nearest_flines)

                # check if the field lines are already plotted, to avoid repetition
                for n, field_line in enumerate(field_lines):
                    color = {0:'black', -1:'tab:blue', 1:'tab:red'}.get(field_line.polarity)
                    coords = field_line.coords
                    coords.representation_type = 'cartesian'
                    x_line = coords.x/aconst.R_sun
                    y_line = coords.y/aconst.R_sun
                    z_line = coords.z/aconst.R_sun

                    # show the closest field lines for the current centroid 
                    if any(closest_fieldlines['num_fieldline'] == n):
                        opacity = 0.2

                        # if they are empty, plot the field lines for the first centroid and append its coords
                        if len(x_lines) == 0 and len(y_lines) == 0 and len(z_lines) == 0:
                            # plot and store the field lines
                            ax.plot3D(x_line, y_line, z_line, color=color, linewidth=1, alpha=opacity, zorder=10)
                            x_lines.append(x_line.value)
                            y_lines.append(y_line.value)
                            z_lines.append(z_line.value)

                        # if they are not empty, go over each array inside and proceed with the checking
                        else:
                            if not any(np.array_equal(i, x_line.value) for i in x_lines) \
                            or not any(np.array_equal(i, y_line.value) for i in y_lines) \
                            or not any(np.array_equal(i, z_line.value) for i in z_lines):
                                ax.plot3D(x_line, y_line, z_line, color=color, linewidth=1, alpha=opacity, zorder=10)
                                x_lines.append(x_line.value)
                                y_lines.append(y_line.value)
                                z_lines.append(z_line.value)
            pbar.update(1)
    
    if fit:
        with tqdm(total=len(cpsX), desc='Fitting the locations of radio sources') as pbar:
            # Generate function out of provided points
            tck, u_param = splprep([cpsX, cpsY, cpsZ], k=3, s=20)
            # Creating spline points
            newPoints = splev(u_param, tck)
            # draw the spline fit curve, represents the trajectory
            ax.plot3D(newPoints[:][0], newPoints[:][1], newPoints[:][2], 'r--')
            pbar.update(1)

    # draw Sun-Earth LOS direction
    x_pos = 2
    y_pos = 0
    z_pos = 0
    x_direct = 1
    y_direct = 0
    z_direct = 0
    ax.quiver(x_pos, y_pos, z_pos, x_direct, y_direct, z_direct, length=2, arrow_length_ratio=0.3, color='black')
    ax.text(x_pos, y_pos, z_pos, 'Earth LOS', size=10, zorder=1, color='black')

    # plot settings
    ax.legend(title='Centroids', loc='upper left', prop={'size':8}, bbox_to_anchor=(-0.01, 1))
    ax.grid(False)
    ax.set_xlim(0, 4)
    ax.set_ylim(-3, 3)
    ax.set_zlim(-3, 3)
    ax.set_box_aspect([2, 3, 3])
    for axis in 'xyz':
        getattr(ax, 'set_{}label'.format(axis))(axis+' [R$_\odot]$')
    if elev==None or azim==None:
        pass
    else:
        ax.view_init(elev=elev, azim=azim)
    if save:
        plt.savefig(f'{basedir_lof}/plots/pfss/pfss3d_contours_{burst_num.split("_")[0]}_elev_{elev}_azim_{azim}.png', dpi=300, format='png', bbox_inches='tight')
        plt.savefig(f'{basedir_lof}/plots/pfss/pfss3d_contours_{burst_num.split("_")[0]}_elev_{elev}_azim_{azim}.pdf', format='pdf', bbox_inches='tight')
    plt.show()







